package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestSkipNumber(t *testing.T) {
	rns := []rune("242342str")
	i := 0
	skipNumber(rns, &i)
	assert.Equal(t, 6, i)

	rns = []rune("str")
	i = 0
	skipNumber(rns, &i)
	assert.Equal(t, 0, i)
}
func TestUnpackString(t *testing.T) {
	res, err := unpackString("a10")

	assert.Nil(t, err)
	assert.Equal(t, "aaaaaaaaaa", res)

	res, err = unpackString("\\5")

	assert.Nil(t, err)
	assert.Equal(t, "5", res)

	res, err = unpackString("ajh\\5s5bcc")

	assert.Nil(t, err)
	assert.Equal(t, "ajh5sssssbcc", res)

	res, err = unpackString("qwe\\4\\5")

	assert.Nil(t, err)
	assert.Equal(t, "qwe45", res)

	res, err = unpackString("qwe\\45")

	assert.Nil(t, err)
	assert.Equal(t, "qwe44444", res)

	res, err = unpackString("qwe\\\\5")

	assert.Nil(t, err)
	assert.Equal(t, "qwe\\\\\\\\\\", res)

	res, err = unpackString("qwe\\45")

	assert.Nil(t, err)
	assert.Equal(t, "qwe44444", res)

	res, err = unpackString("qwe\\0")

	assert.Nil(t, err)
	assert.Equal(t, "qwe0", res)

	res, err = unpackString("qwe\\00")

	assert.Nil(t, err)
	assert.Equal(t, "qwe", res)

	_, err = unpackString("45")
	assert.NotNil(t, err)

	res, err = unpackString("d")
	assert.Nil(t, err)
	assert.Equal(t, "d", res)

	res, err = unpackString("")
	assert.Nil(t, err)
	assert.Equal(t, "", res)
}
