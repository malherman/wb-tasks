package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"strconv"
	"time"
)

// Event содержит данные об ивенте
type Event struct {
	ID          int       `json:"event_id"`
	Discription string    `json:"disctipion"`
	Date        time.Time `json:"date"`
}

// User содержит инфу о User
type User struct {
	ID     int           `json:"user_id"`
	Events map[int]Event `json:"events"`
}

// Server представляет собой сервер
type Server struct {
	Logger  *log.Logger
	EventID int          `json:"event_id"`
	Users   map[int]User `json:"users"`
}

// EventsResult - это результат запроса
type EventsResult struct {
	Result string  `json:"result"`
	Events []Event `json:"events"`
}

func main() {
	s := Server{Users: map[int]User{}, Logger: log.Default()}

	http.Handle("/create_event", s.Middleware(s.CreateEvent))
	http.Handle("/update_event", s.Middleware(s.UpdateEvent))
	http.Handle("/delete_event", s.Middleware(s.DeleteEvent))
	http.Handle("/events_for_day", s.Middleware(s.EventsForDay))
	http.Handle("/events_for_week", s.Middleware(s.EventsForWeek))
	http.Handle("/events_for_month", s.Middleware(s.EventsForMonth))

	s.Logger.Println("Starting on :8080")
	fmt.Fprintln(os.Stderr, http.ListenAndServe(":8080", nil))
}

// Middleware промежуточное звено между приемником запроса и обработчиком
func (s *Server) Middleware(nextFunc http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		s.Logger.Println("Request from ", r.URL)
		err := r.ParseForm()
		if err != nil {
			s.handleError(w, r, "не удается распарсить запрос POST", http.StatusBadRequest)
			return
		}
		switch r.Method {
		case http.MethodPost:
			s.Logger.Println("Method POST. Values: ", r.PostForm)
		case http.MethodGet:
			s.Logger.Println("Method GET. Values: ", r.URL.RawQuery)
		default:
			s.Logger.Println("Unknown method")
		}
		nextFunc(w, r)
		s.Logger.Println("status of users: ", s.Users)
	}
}

// CreateEvent ...
func (s *Server) CreateEvent(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodPost:
		sUserID := r.PostFormValue("user_id")
		date := r.PostFormValue("date")
		eventDisctiption := r.PostFormValue("discription")

		if sUserID == "" || date == "" {
			s.handleError(w, r, "невалидный метод запроса", http.StatusBadRequest)
			return
		}

		timeDate, err := time.Parse("2006-01-02", date)
		if err != nil {
			s.handleError(w, r, err.Error(), http.StatusBadRequest)
			return
		}

		userID, err := strconv.Atoi(sUserID)
		if err != nil {
			s.handleError(w, r, err.Error(), http.StatusBadRequest)
			return
		}

		eventID := s.generateEventID()
		if _, isExist := s.Users[userID]; !isExist {
			s.Users[userID] = User{
				ID: userID,
				Events: map[int]Event{
					eventID: {
						ID:          eventID,
						Discription: eventDisctiption,
						Date:        timeDate,
					},
				},
			}
		} else {
			s.Users[userID].Events[eventID] = Event{
				ID:          eventID,
				Discription: eventDisctiption,
				Date:        timeDate,
			}
		}
		s.handleOK(w, r, fmt.Sprintf("ивент успешно добавлен с id %d", eventID))
	default:
		s.handleError(w, r, "Невалидный метод запроса", http.StatusBadRequest)
		return
	}
}

// UpdateEvent ...
func (s *Server) UpdateEvent(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodPost:
		sUserID := r.PostFormValue("user_id")
		sEventID := r.PostFormValue("event_id")

		if sUserID == "" || sEventID == "" {
			s.handleError(w, r, "невалидный запрос", http.StatusBadRequest)
			return
		}

		userID, err := strconv.Atoi(sUserID)
		if err != nil {
			s.handleError(w, r, "неверно указан user_id", http.StatusBadRequest)
			return
		}

		eventID, err := strconv.Atoi(sEventID)
		if err != nil {
			s.handleError(w, r, "неверно указан event_id", http.StatusBadRequest)
			return
		}

		if _, isExist := s.Users[userID]; !isExist {
			s.handleError(w, r, "пользователя с таким id не существует", http.StatusBadRequest)
			return
		}
		event, isExists := s.Users[userID].Events[eventID]
		if !isExists {
			s.handleError(w, r, "ивента с таким event_id не существует", http.StatusBadRequest)
			return
		}

		date, disc := r.PostForm["date"], r.PostForm["discription"]
		if len(date) != 0 && date[0] != "" {
			event.Date, err = time.Parse("2006-01-02", date[0])
			if err != nil {
				s.handleError(w, r, "невалидная date", http.StatusBadRequest)
				return
			}
		}

		if len(disc) != 0 {
			event.Discription = disc[0]
		}

		s.Users[userID].Events[eventID] = event

		s.handleOK(w, r, fmt.Sprintf("ивент успешно обновлен с id %d", eventID))
	default:
		s.handleError(w, r, "Невалидный метод запроса", http.StatusBadRequest)
		return
	}
}

// DeleteEvent ...
func (s *Server) DeleteEvent(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodPost:
		sUserID := r.PostFormValue("user_id")
		sEventID := r.PostFormValue("event_id")

		if sUserID == "" || sEventID == "" {
			s.handleError(w, r, "невалидный запрос", http.StatusBadRequest)
			return
		}

		userID, err := strconv.Atoi(sUserID)
		if err != nil {
			s.handleError(w, r, "неверно указан user_id", http.StatusBadRequest)
			return
		}

		eventID, err := strconv.Atoi(sEventID)
		if err != nil {
			s.handleError(w, r, "неверно указан event_id", http.StatusBadRequest)
			return
		}

		if _, isExist := s.Users[userID]; !isExist {
			s.handleError(w, r, "пользователя с таким id не существует", http.StatusBadRequest)
			return
		}

		delete(s.Users[userID].Events, eventID)
		s.handleOK(w, r, fmt.Sprintf("ивент успешно удален под id %d", eventID))
	default:
		s.handleError(w, r, "Невалидный метод запроса", http.StatusBadRequest)
	}
}

// EventsForDay ...
func (s *Server) EventsForDay(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodGet:
		sMonthNumber := r.Form.Get("day")

		if sMonthNumber == "" {
			s.handleError(w, r, "не указан day", http.StatusBadRequest)
			return
		}

		monthNumber, err := strconv.Atoi(sMonthNumber)
		if err != nil {
			s.handleError(w, r, "невалидный day", http.StatusBadRequest)
		}

		eventsForDay := []Event{}
		for _, user := range s.Users {
			for _, event := range user.Events {
				if event.Date.Day() == monthNumber {
					eventsForDay = append(eventsForDay, event)
				}
			}
		}

		result := EventsResult{
			Result: "успех",
			Events: eventsForDay,
		}

		w.Header().Set("Content-Type", "application/json; charset=utf-8")
		w.WriteHeader(http.StatusOK)
		err = json.NewEncoder(w).Encode(&result)
		if err != nil {
			s.handleError(w, r, "ошибка сериализации данных", http.StatusInternalServerError)
			return
		}
	default:
		s.handleError(w, r, "Невалидный метод запроса", http.StatusBadRequest)
	}
}

// EventsForWeek ...
func (s *Server) EventsForWeek(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodGet:
		sWeekNumber := r.Form.Get("week")

		if sWeekNumber == "" {
			s.handleError(w, r, "не указан week", http.StatusBadRequest)
			return
		}

		weekNumber, err := strconv.Atoi(sWeekNumber)
		if err != nil {
			s.handleError(w, r, "невалидный day", http.StatusBadRequest)
		}

		eventsForWeek := []Event{}
		for _, user := range s.Users {
			for _, event := range user.Events {
				if (event.Date.Day()-1)/7+1 == weekNumber {
					eventsForWeek = append(eventsForWeek, event)
				}
			}
		}

		result := EventsResult{
			Result: "успех",
			Events: eventsForWeek,
		}

		w.Header().Set("Content-Type", "application/json; charset=utf-8")
		w.WriteHeader(http.StatusOK)
		err = json.NewEncoder(w).Encode(&result)
		if err != nil {
			s.handleError(w, r, "ошибка сериализации данных", http.StatusInternalServerError)
			return
		}
	default:
		s.handleError(w, r, "Невалидный метод запроса", http.StatusBadRequest)
	}
}

// EventsForMonth ...
func (s *Server) EventsForMonth(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodGet:
		sMonthNumber := r.Form.Get("month")

		if sMonthNumber == "" {
			s.handleError(w, r, "не указан month", http.StatusBadRequest)
			return
		}

		monthNumber, err := strconv.Atoi(sMonthNumber)
		if err != nil {
			s.handleError(w, r, "невалидный month", http.StatusBadRequest)
		}

		eventsForMonth := []Event{}
		for _, user := range s.Users {
			for _, event := range user.Events {
				if int(event.Date.Month()) == monthNumber {
					eventsForMonth = append(eventsForMonth, event)
				}
			}
		}

		result := EventsResult{
			Result: "успех",
			Events: eventsForMonth,
		}

		w.Header().Set("Content-Type", "application/json; charset=utf-8")
		w.WriteHeader(http.StatusOK)
		err = json.NewEncoder(w).Encode(&result)
		if err != nil {
			s.handleError(w, r, "ошибка сериализации данных", http.StatusInternalServerError)
			return
		}
	default:
		s.handleError(w, r, "Невалидный метод запроса", http.StatusBadRequest)
	}
}

func (s *Server) generateEventID() int {
	defer func() {
		s.EventID++
	}()
	return s.EventID
}

func (s *Server) handleError(w http.ResponseWriter, r *http.Request, msg string, statusCode int) {
	s.Logger.Println(r.URL, " "+msg)
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(statusCode)
	_, err := w.Write([]byte(fmt.Sprintf("{\"error\": \"%s\"}", msg)))
	if err != nil {
		s.Logger.Println(err)
		return
	}
}

func (s *Server) handleOK(w http.ResponseWriter, r *http.Request, msg string) {
	s.Logger.Println(r.URL, " "+msg)
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(http.StatusOK)
	_, err := w.Write([]byte(fmt.Sprintf("{\"result\": \"%s\"}", msg)))
	if err != nil {
		s.Logger.Println(err)
		return
	}
}
