package main

import (
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestJust(t *testing.T) {
	assert.True(t, '\n' < '{')
	assert.True(t, 'a' < '{')
	assert.True(t, 'b' < '{')
	assert.True(t, 'b' > ' ')
	assert.Greater(t, '{', ' ')
	assert.Greater(t, '{', '\n')
}

func TestStringsEqualFold(t *testing.T) {
	assert.Equal(t, true, strings.EqualFold("Hello", "hello"))
}

func TestStringsCompare(t *testing.T) {
	s1 := "Hello"
	s2 := "hello"

	assert.Equal(t, 1, stringsCompare(&s1, &s2))
	assert.Equal(t, -1, stringsCompare(&s2, &s1))

	s1 = " Hello"
	s2 = "hello"

	assert.Equal(t, -1, stringsCompare(&s1, &s2))
	assert.Equal(t, 1, stringsCompare(&s2, &s1))

	s1 = " Hello"
	s2 = "5hello"

	assert.Equal(t, -1, stringsCompare(&s1, &s2))
	assert.Equal(t, 1, stringsCompare(&s2, &s1))

	s1 = "5Hello"
	s2 = "hello"

	assert.Equal(t, -1, stringsCompare(&s1, &s2))
	assert.Equal(t, 1, stringsCompare(&s2, &s1))

	s1 = "Hello"
	s2 = "Hello"

	assert.Equal(t, 0, stringsCompare(&s1, &s2))
	assert.Equal(t, 0, stringsCompare(&s2, &s1))
}
