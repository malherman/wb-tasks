package main

import (
	"bufio"
	"bytes"
	"fmt"
	"os"
	"os/exec"
	"os/user"
	"strings"
)

// Shell содержит инфу по состоянию шела
type Shell struct {
	currentDirectory string
	homeDirPath      string
	psPath           string
}

//ProcessInfo содержит инфу об определенном процессе
type ProcessInfo struct {
	pid int
	tty int
	cmd string
}

var shell Shell

// при старте программы
// заполняем инфу о шеле
func init() {
	currentDir, err := os.Executable()
	if err != nil {
		panic(err)
	}

	user, err := user.Current()
	if err != nil {
		panic(err)
	}
	shell = Shell{
		currentDirectory: currentDir,
		homeDirPath:      user.HomeDir,
	}

	shell.psPath, err = getCommandPath("ps")
}

// возращает путь к нужной программе в os, либо ошибку
func getCommandPath(cmd string) (path string, err error) {
	buffer := bytes.NewBuffer([]byte{})
	process := exec.Command("which", cmd)
	process.Stdout = buffer

	err = process.Run()
	if err != nil {
		return
	}

	path = buffer.String()
	return
}

func main() {
	var (
		command string
		err     error
		scanner = bufio.NewScanner(os.Stdin)
	)

	fmt.Print("$ ")
	for scanner.Scan() {
		// сканируем кажду строку
		command = scanner.Text()
		splitedCommand := strings.Split(command, " ")
		if len(splitedCommand) == 0 {
			fmt.Print("$ ")
			continue
		}
		switch splitedCommand[0] {
		// если пользователь ввел cd
		case "cd":
			// если не указал директорию
			if len(splitedCommand) == 1 {
				// переходим в домашнюю директорию
				if err = checkPath(shell.homeDirPath); err != nil {
					fmt.Fprintln(os.Stderr, err)
					break
				}
				shell.currentDirectory = shell.homeDirPath
				break
			}
			// иначе устанавливаем в качестве текущей директории ту,
			// которую указал пользователь
			if err = checkPath(splitedCommand[1]); err != nil {
				fmt.Fprintln(os.Stderr, err)
				break
			}
			shell.currentDirectory = splitedCommand[1]
			// вывод текущей директории
		case "pwd":
			fmt.Println(shell.currentDirectory)
			// просто эхо строк
		case "echo":
			for _, str := range os.Args[1:] {
				fmt.Print(str, " ")
			}
			fmt.Println()
			// убиваем процесс
		case "kill":
			// если не указан pid процесса
			if len(splitedCommand) <= 1 {
				fmt.Fprintln(os.Stderr, "not enough arguments for killing")
				break
			}
			pid := splitedCommand[1]
			command := exec.Command("kill", pid)
			command.Stdout = os.Stdout
			command.Stderr = os.Stderr
			command.Run()
			// список процессов текущего шела
		case "ps":
			command := exec.Command("ps")
			command.Stdout = os.Stdout
			err = command.Run()
			if err != nil {
				fmt.Fprintln(os.Stderr, err)
				break
			}
			// завершение работы шела
		case "exit":
			os.Exit(0)
			// неизвестная команда
		default:
			fmt.Fprintln(os.Stderr, "command not found...")
		}
		fmt.Print("$ ")
	}
}

// проверяет корректность пути к директории
func checkPath(dir string) error {
	info, err := os.Stat(dir)
	if os.IsNotExist(err) {
		return fmt.Errorf("%s is not exist", dir)
	} else if !info.IsDir() {
		return fmt.Errorf("%s is not a directory", dir)
	} else if err != nil {
		return err
	}

	return nil
}
