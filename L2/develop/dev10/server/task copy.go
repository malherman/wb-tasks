package main

import (
	"bufio"
	"flag"
	"fmt"
	"net"
	"os"
	"regexp"
	"strconv"
	"time"
)

var (
	timeout    int
	sTimeout   string
	timeType   rune
	host, port string
)

func init() {
	flag.StringVar(&sTimeout, "timeout", "10s", "timeout value")
}

func main() {
	flag.Parse()
	err := parseTimeout()
	if err != nil {
		fmt.Fprintln(os.Stderr, err.Error())
		return
	}

	// if len(flag.Args()) == 0 {
	// 	fmt.Fprintln(os.Stderr, "не указан host")
	// 	os.Exit(1)
	// }

	// if len(flag.Args()) == 1 {
	// 	fmt.Fprintln(os.Stderr, "не указан port")
	// 	os.Exit(1)
	// }

	// if len(flag.Args()) > 2 {
	// 	fmt.Fprintln(os.Stderr, "слишком много аргументов")
	// 	os.Exit(1)
	// }

	serverListener, err := net.Listen("tcp", ":8080")
	if err != nil {
		fmt.Fprintln(os.Stderr, err.Error())
		return
	}

	conn, err := serverListener.Accept()
	if err != nil {
		fmt.Fprintln(os.Stderr, err.Error())
		return
	}

	fmt.Println(conn.RemoteAddr().String())

	go func() {
		reader := bufio.NewReader(os.Stdin)
		var buffer []byte
		for {
			buffer, err = reader.ReadBytes('\n')
			conn.Write(buffer)
			fmt.Println("Circle")
		}
	}()

	reader := bufio.NewReader(conn)
	var buffer []byte
	for {
		buffer, err = reader.ReadBytes('\n')
		if err != nil {
			break
		}
		fmt.Print(string(buffer))
	}

	return
	host, port = flag.Args()[0], flag.Args()[1]
	listener, err := net.Listen("tcp", host+":"+port)
	if err != nil {
		fmt.Fprintln(os.Stderr, err.Error())
		os.Exit(1)
	}

	defer listener.Close()

	var timer *time.Timer
	switch timeType {
	case 's':
		timer = time.NewTimer(time.Duration(timeout) * time.Second)
	case 'm':
		timer = time.NewTimer(time.Duration(timeout) * time.Minute)
	case 'h':
		timer = time.NewTimer(time.Duration(timeout) * time.Hour)
	}

	doneChan, errChan := make(chan struct{}), make(chan error)
	go handleConnection(listener, doneChan, errChan)
	select {
	case <-doneChan:
		break
	case <-timer.C:
		fmt.Fprintln(os.Stderr, "time is out. Closing...")
		os.Exit(1)
	case err = <-errChan:
		fmt.Fprintln(os.Stderr, err.Error())
		os.Exit(1)
	}

	fmt.Println(timeout, string(timeType))
	<-doneChan
}

func parseTimeout() error {
	doesContain, err := regexp.MatchString(`s$`, sTimeout)
	if err != nil {
		return err
	}

	if doesContain {
		rTimeout := []rune(sTimeout)
		timeout, err = strconv.Atoi(string(rTimeout[:len(rTimeout)-1]))
		timeType = 's'
		if err != nil {
			return err
		}
	}

	doesContain, err = regexp.MatchString(`m$`, sTimeout)
	if err != nil {
		return err
	}

	if doesContain {
		rTimeout := []rune(sTimeout)
		timeout, err = strconv.Atoi(string(rTimeout[:len(rTimeout)-1]))
		timeType = 'm'
		if err != nil {
			return err
		}
	}

	doesContain, err = regexp.MatchString(`h$`, sTimeout)
	if err != nil {
		return err
	}

	if doesContain {
		rTimeout := []rune(sTimeout)
		timeout, err = strconv.Atoi(string(rTimeout[:len(rTimeout)-1]))
		timeType = 'h'
		if err != nil {
			return err
		}
	}

	return nil
}

func handleConnection(listener net.Listener, doneChan chan struct{}, errChan chan error) {
	conn, err := listener.Accept()
	if err != nil {
		errChan <- err
		return
	}

	doneChan <- struct{}{}
	go readingHandle(conn, doneChan)
	go writingHandle(conn, doneChan)
}

func readingHandle(conn net.Conn, finishChan chan struct{}) {
	buffer := []byte{}
	for {
		n, err := conn.Read(buffer)
		if err != nil {
			finishChan <- struct{}{}
			return
		}
		if n > 0 {
			fmt.Println(string(buffer))
		}
	}
}

func writingHandle(conn net.Conn, finishChan chan struct{}) {
	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		conn.Write(scanner.Bytes())
	}
	finishChan <- struct{}{}
}
