package main

import (
	"bufio"
	"flag"
	"fmt"
	"net"
	"os"
	"regexp"
	"strconv"
	"time"
)

var (
	timeout    int
	sTimeout   string
	timeType   rune
	host, port string
)

// инициализация таймаута для соединения
func init() {
	flag.StringVar(&sTimeout, "timeout", "10s", "timeout value")
}

func main() {
	flag.Parse()
	// парсим таймаут
	err := parseTimeout()
	if err != nil {
		fmt.Fprintln(os.Stderr, err.Error())
		return
	}

	// если не указан хост
	if len(flag.Args()) == 0 {
		fmt.Fprintln(os.Stderr, "не указан host")
		os.Exit(1)
	}

	// если не указан порт
	if len(flag.Args()) == 1 {
		fmt.Fprintln(os.Stderr, "не указан port")
		os.Exit(1)
	}

	// если присутствуют лишние аргументы
	if len(flag.Args()) > 2 {
		fmt.Fprintln(os.Stderr, "слишком много аргументов")
		os.Exit(1)
	}

	host, port = flag.Args()[0], flag.Args()[1]

	// создаем таймер и инициализируем
	var timer *time.Timer
	switch timeType {
	case 's':
		timer = time.NewTimer(time.Duration(timeout) * time.Second)
	case 'm':
		timer = time.NewTimer(time.Duration(timeout) * time.Minute)
	case 'h':
		timer = time.NewTimer(time.Duration(timeout) * time.Hour)
	}

	// deadline указывает, до какого момента будем ждать соединения
	deadline := time.Now().Add(time.Duration(timeout) * time.Second)
	conn, err := net.Dial("tcp", host+":"+port)
	for err != nil && time.Now().Before(deadline) {
		conn, err = net.Dial("tcp", host+":"+port)
	}

	// deadline наступил
	if err != nil {
		fmt.Fprintln(os.Stderr, "Time is out")
		os.Exit(1)
	}

	fmt.Println("Connection is done")
	defer conn.Close()

	doneChan, errChan := make(chan struct{}), make(chan error)
	go handleConnection(conn, doneChan, errChan)
	// ждем сигнал, что все готов
	// либо таймаут
	// либо ошибку
	select {
	case <-doneChan:
		break
	case <-timer.C:
		fmt.Fprintln(os.Stderr, "time is out. Closing...")
		return
	case err = <-errChan:
		fmt.Fprintln(os.Stderr, err.Error())
		return
	}
	// ждем окончания соединения
	<-doneChan
	fmt.Println("Program is finishing")
}

// парсит таймаут согласно --timeout=3s
func parseTimeout() error {
	doesContain, err := regexp.MatchString(`s$`, sTimeout)
	if err != nil {
		return err
	}

	if doesContain {
		rTimeout := []rune(sTimeout)
		timeout, err = strconv.Atoi(string(rTimeout[:len(rTimeout)-1]))
		timeType = 's'
		if err != nil {
			return err
		}
	}

	doesContain, err = regexp.MatchString(`m$`, sTimeout)
	if err != nil {
		return err
	}

	if doesContain {
		rTimeout := []rune(sTimeout)
		timeout, err = strconv.Atoi(string(rTimeout[:len(rTimeout)-1]))
		timeType = 'm'
		if err != nil {
			return err
		}
	}

	doesContain, err = regexp.MatchString(`h$`, sTimeout)
	if err != nil {
		return err
	}

	if doesContain {
		rTimeout := []rune(sTimeout)
		timeout, err = strconv.Atoi(string(rTimeout[:len(rTimeout)-1]))
		timeType = 'h'
		if err != nil {
			return err
		}
	}

	return nil
}

// стартует соединения
func handleConnection(conn net.Conn, doneChan chan struct{}, errChan chan error) {
	doneChan <- struct{}{}
	go readingHandle(conn, doneChan)
	go writingHandle(conn, doneChan)
}

// читаем из сокета, пишем в stdout
func readingHandle(conn net.Conn, finishChan chan struct{}) {
	reader := bufio.NewReader(conn)
	for {
		buffer, err := reader.ReadBytes('\n')
		if err != nil {
			break
		}
		fmt.Print(string(buffer))
	}
	finishChan <- struct{}{}
}

// читаем из stdout, пишем в сокет conn
func writingHandle(conn net.Conn, finishChan chan struct{}) {
	reader := bufio.NewReader(os.Stdin)
	for {
		buffer, err := reader.ReadBytes('\n')
		if err != nil || len(buffer) == 0 {
			break
		}
		conn.Write(buffer)
	}
	finishChan <- struct{}{}
}
