package main

import (
	"bufio"
	"flag"
	"fmt"
	"os"
	"sort"
	"strconv"
	"strings"
)

type fOption []int

func (fo *fOption) String() string {
	return fmt.Sprint(*fo)
}

// записываем все номера столбцов
// из аргумента в нужный слайс
func (fo *fOption) Set(v string) error {
	splited := strings.Split(v, ",")
	for i := range splited {
		iV, err := strconv.Atoi(splited[i])
		if err != nil {
			return err
		}

		*fo = append(*fo, iV)
	}

	return nil
}

// Options ...
type Options struct {
	f fOption
	d string
	s bool
}

var options = Options{
	d: "\t",
}

func init() {
	flag.Var(&options.f, "f", "выбрать поля (колонки)")
	flag.StringVar(&options.d, "d", "\t", "использовать другой разделитель")
	flag.BoolVar(&options.s, "s", false, "только строки с разделителем")
}

func main() {
	flag.Parse()
	inputSource := os.Stdin

	sort.Ints(options.f)
	if len(options.f) == 0 {
		fmt.Fprintln(os.Stderr, "No f flag")
		return
	}

	if options.f[0] <= 0 {
		fmt.Fprintln(os.Stderr, "cut: поля нумеруются с 1")
		return
	}
	performCut(&options, inputSource)
}

// выполняет разбиение строк по разделителю на колонки
// и выводит запрошенные
func performCut(opt *Options, inputSource *os.File) error {
	fileLines := readFileLines(inputSource)
	splitedFileLines := make([][]string, 0, len(fileLines))

	for i := range fileLines {
		if opt.s && strings.Contains(fileLines[i], opt.d) || !opt.s {
			splitedFileLines = append(splitedFileLines, strings.Split(fileLines[i], opt.d))
		}
	}

	for i := range splitedFileLines {
		for j := range opt.f {
			if opt.f[j] <= len(splitedFileLines[i]) {
				fmt.Print(splitedFileLines[i][opt.f[j]-1])
				if j < len(opt.f)-1 {
					fmt.Print(opt.d)
				}
			}
		}
		fmt.Println()
	}
	return nil
}

// читаем строки файла и возращаем
func readFileLines(file *os.File) (res []string) {
	scanner := bufio.NewScanner(file)

	res = []string{}
	for scanner.Scan() {
		res = append(res, scanner.Text())
	}

	return
}
