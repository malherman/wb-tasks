package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"regexp"
	"strings"

	"golang.org/x/net/html"
)

// SourceURL содержит исходный URL, который ввел пользователь
var SourceURL *url.URL
var visitedPages = map[string]struct{}{}

// Link содержит инфу об адресе
type Link struct {
	CanBeDownloaded bool
	Address         *url.URL
}

func main() {
	// проверка аргументов
	if len(os.Args) != 2 {
		fmt.Fprintln(os.Stderr, "not enouge arguments")
		os.Exit(1)
	}

	// парсим url, который ввел пользователь
	u, err := url.Parse(os.Args[1])
	if err != nil {
		fmt.Fprintln(os.Stderr, err.Error())
		os.Exit(1)
	}

	// пробуем протокол https
	u.Scheme = "https"
	_, err = http.Get(u.String())
	// если не прокатило, то http
	if err != nil {
		u.Scheme = "http"
	}

	// Помещаем в SourceURL исходный адрес
	SourceURL, err = url.Parse(u.String())
	if err != nil {
		fmt.Fprintln(os.Stderr, err.Error())
		os.Exit(1)
	}

	// удаляем www из SourceURL
	err = removeWWWFromURL(SourceURL)
	if err != nil {
		fmt.Fprintln(os.Stderr, err.Error())
		os.Exit(2)
	}

	// выполняем обработку адреса SourceURL
	err = addrHandle(SourceURL)
	if err != nil {
		fmt.Fprintln(os.Stderr, err.Error())
		os.Exit(3)
	}
}

// из каждого тега вытаскиваем ссылку href или src
func getLinkFromTeg(n []html.Attribute) (isFound bool, link *url.URL, err error) {
	for i := range n {
		switch n[i].Key {
		case "href":
			link, err = url.Parse(n[i].Val)
			if err != nil {
				continue
			}
			err = removeWWWFromURL(link)
			if err != nil {
				return
			}
			isFound = true
			return
		case "src":
			link, err = url.Parse(n[i].Val)
			if err != nil {
				continue
			}
			err = removeWWWFromURL(link)
			if err != nil {
				return
			}
			isFound = true
			return
		}
	}
	return
}

func addrHandle(pageAddr *url.URL) error {
	var (
		err             error
		tkn             *html.Tokenizer
		addr            *url.URL
		resp            *http.Response
		tt              html.TokenType
		t               html.Token
		isFound         bool
		links           []Link
		canBeDownloaded bool
	)

	resp, err = makeRespToURL(pageAddr)
	if err != nil {
		return err
	}

	defer resp.Body.Close()

	contentBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	dirPath := pageAddr.Hostname() + pageAddr.Path

	err = os.MkdirAll(dirPath, 0777)
	if err != nil {
		return err
	}

	err = ioutil.WriteFile(dirPath+"/"+"index.html", contentBytes, 0777)
	if err != nil {
		return err
	}

	fmt.Println(pageAddr)
	visitedPages[pageAddr.String()] = struct{}{}
	tkn = html.NewTokenizer(resp.Body)
	// из тега, который может содержать ссылку href или src
	// вытаскиваем ссылки и помещаем в слайс links
HANDLE_LOOP:
	for {
		tt = tkn.Next()
		switch tt {
		case html.StartTagToken:
			canBeDownloaded = false
			isFound = false
			t = tkn.Token()
			switch t.Data {
			case "a":
				isFound, addr, err = getLinkFromTeg(t.Attr)
				if err != nil {
					fmt.Fprintln(os.Stderr, err.Error())
					continue HANDLE_LOOP
				}
				if !isFound || addr.Hostname() != SourceURL.Hostname() {
					continue HANDLE_LOOP
				}
			case "link":
				isFound, addr, err = getLinkFromTeg(t.Attr)
				if err != nil {
					fmt.Fprintln(os.Stderr, err.Error())
					continue HANDLE_LOOP
				}
				if !isFound || addr.Hostname() != SourceURL.Hostname() {
					continue HANDLE_LOOP
				} else if addr.Hostname() == "" {
					addr.Host = SourceURL.Host
					addr.Scheme = SourceURL.Scheme
				}
				fmt.Println(addr)
				canBeDownloaded = true
			case "img":
				isFound, addr, err = getLinkFromTeg(t.Attr)
				if err != nil {
					fmt.Fprintln(os.Stderr, err.Error())
					continue HANDLE_LOOP
				}
				if !isFound || addr.Hostname() != SourceURL.Hostname() {
					continue HANDLE_LOOP
				} else if addr.Hostname() == "" {
					addr.Host = SourceURL.Host
					addr.Scheme = SourceURL.Scheme
				}
				canBeDownloaded = true
			case "audio":
				isFound, addr, err = getLinkFromTeg(t.Attr)
				if err != nil {
					fmt.Fprintln(os.Stderr, err.Error())
					continue HANDLE_LOOP
				}
				if !isFound || addr.Hostname() != SourceURL.Hostname() {
					continue HANDLE_LOOP
				} else if addr.Hostname() == "" {
					addr.Host = SourceURL.Host
					addr.Scheme = SourceURL.Scheme
				}
				canBeDownloaded = true
			case "embed":
				isFound, addr, err = getLinkFromTeg(t.Attr)
				if err != nil {
					fmt.Fprintln(os.Stderr, err.Error())
					continue HANDLE_LOOP
				}
				if !isFound || addr.Hostname() != SourceURL.Hostname() {
					continue HANDLE_LOOP
				} else if addr.Hostname() == "" {
					addr.Host = SourceURL.Host
					addr.Scheme = SourceURL.Scheme
				}
				canBeDownloaded = true
			case "iframe":
				isFound, addr, err = getLinkFromTeg(t.Attr)
				if err != nil {
					fmt.Fprintln(os.Stderr, err.Error())
					continue HANDLE_LOOP
				}
				if !isFound || addr.Hostname() != SourceURL.Hostname() {
					continue HANDLE_LOOP
				} else if addr.Hostname() == "" {
					addr.Host = SourceURL.Host
					addr.Scheme = SourceURL.Scheme
				}
				canBeDownloaded = true
			case "input":
				isFound, addr, err = getLinkFromTeg(t.Attr)
				if err != nil {
					fmt.Fprintln(os.Stderr, err.Error())
					continue HANDLE_LOOP
				}
				if !isFound || addr.Hostname() != SourceURL.Hostname() {
					continue HANDLE_LOOP
				} else if addr.Hostname() == "" {
					addr.Host = SourceURL.Host
					addr.Scheme = SourceURL.Scheme
				}
				canBeDownloaded = true
			case "script":
				isFound, addr, err = getLinkFromTeg(t.Attr)
				if err != nil {
					fmt.Fprintln(os.Stderr, err.Error())
					continue HANDLE_LOOP
				}
				if !isFound || addr.Hostname() != SourceURL.Hostname() {
					continue HANDLE_LOOP
				} else if addr.Hostname() == "" {
					addr.Host = SourceURL.Host
					addr.Scheme = SourceURL.Scheme
				}
				canBeDownloaded = true
			case "source":
				isFound, addr, err = getLinkFromTeg(t.Attr)
				if err != nil {
					fmt.Fprintln(os.Stderr, err.Error())
					continue HANDLE_LOOP
				}
				if !isFound || addr.Hostname() != SourceURL.Hostname() {
					fmt.Println(addr.Hostname())
					continue HANDLE_LOOP
				} else if addr.Hostname() == "" {
					addr.Host = SourceURL.Host
					addr.Scheme = SourceURL.Scheme
				}
				canBeDownloaded = true
			case "track":
				isFound, addr, err = getLinkFromTeg(t.Attr)
				if err != nil {
					fmt.Fprintln(os.Stderr, err.Error())
					continue HANDLE_LOOP
				}
				if !isFound || addr.Hostname() != SourceURL.Hostname() {
					fmt.Println(addr.Hostname())
					continue HANDLE_LOOP
				} else if addr.Hostname() == "" {
					addr.Host = SourceURL.Host
					addr.Scheme = SourceURL.Scheme
				}
				canBeDownloaded = true
			case "video":
				isFound, addr, err = getLinkFromTeg(t.Attr)
				if err != nil {
					fmt.Fprintln(os.Stderr, err.Error())
					continue HANDLE_LOOP
				}
				if !isFound || addr.Hostname() != SourceURL.Hostname() {
					fmt.Println(addr.Hostname())
					continue HANDLE_LOOP
				} else if addr.Hostname() == "" {
					addr.Host = SourceURL.Host
					addr.Scheme = SourceURL.Scheme
				}
				canBeDownloaded = true
			}
			if isFound {
				links = append(links, Link{CanBeDownloaded: canBeDownloaded, Address: addr})
			}
		case html.ErrorToken:
			break HANDLE_LOOP
		}
	}

	// пробегаемся по каждой ссылке из linkk,
	// создаем нужную структуру папок
	// загружаем файлы
	for i := range links {

		if links[i].CanBeDownloaded {
			fmt.Println("Making dir for url: ", links[i].Address)
			err = makeDirsPathFromURL(links[i].Address)
			if err != nil {
				fmt.Fprintln(os.Stderr, err.Error())
				continue
			}
			fmt.Println("Downloading file: ", links[i].Address.String())
			err = downloadFile(links[i].Address, links[i].Address.Hostname()+links[i].Address.Path)
			if err != nil {
				fmt.Fprintln(os.Stderr, err.Error())
				continue
			}
		} else if _, isVisited := visitedPages[links[i].Address.String()]; !isVisited {
			fmt.Println("Making dir for url: ", links[i].Address)
			err = makeDirsPathFromURL(links[i].Address)
			if err != nil {
				fmt.Fprintln(os.Stderr, err.Error())
				continue
			}
			fmt.Println("Downloading file: ", links[i].Address.String())
			err = downloadFile(links[i].Address, links[i].Address.Hostname()+links[i].Address.Path+"/"+"index.html")
			if err != nil {
				fmt.Fprintln(os.Stderr, err.Error())
				continue
			}
			err = addrHandle(links[i].Address)
			if err != nil {
				fmt.Fprintln(os.Stderr, err.Error())
				continue
			}
		}
	}
	return nil
}

// подбирает нужный проток
func makeRespToURL(u *url.URL) (resp *http.Response, err error) {
	u.Scheme = "https"
	resp, err = http.Get(u.String())
	if err != nil {
		u.Scheme = "http"
		resp, err = http.Get(u.String())
	}
	return
}

// конкатенация строк из strs по разделителю sep
func stringsSum(strs []string, sep string) (res string) {
	res = ""
	for i := range strs {
		res += strs[i]
		if i < len(strs)-1 {
			res += sep
		}
	}
	return
}

// качаем файл
func downloadFile(link *url.URL, path string) error {
	resp, err := http.Get(link.String())
	if err != nil {
		return err
	}

	defer resp.Body.Close()

	contentBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}
	err = ioutil.WriteFile(path, contentBytes, 0777)
	if err != nil {
		return err
	}
	return nil
}

// строит структуру папок по url
func makeDirsPathFromURL(u *url.URL) error {
	splitedURL := strings.Split(u.Hostname()+u.Path, "/")
	path := stringsSum(splitedURL[:len(splitedURL)-1], "/")
	err := os.MkdirAll(path, 0777)
	if err != nil {
		return err
	}
	return nil
}

// удаляет www из url
func removeWWWFromURL(u *url.URL) error {
	isContain, err := regexp.MatchString(`^www.`, u.Host)
	if err != nil {
		return err
	}
	if isContain {
		u.Host = strings.Replace(u.Host, "www.", "", 1)
	}
	return nil
}
