// сложное сделать простым

package main

import "fmt"

func sayHelloTo(to string) error {
	_, err := fmt.Printf("Hello, %s\n", to)
	return err
}

func checkPanic(err error) {
	if err != nil {
		panic(err)
	}
}

func main() {
	checkPanic(sayHelloTo("Username"))
}
