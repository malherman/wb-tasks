package main

import "fmt"

type Storage struct {
	FreeSpace int
	FileSaver IFileSaver
}

func (s *Storage) SaveFile(filepath string, needSpace int) {
	fmt.Println("Пробуем сохранить файл ", filepath)
	s.FileSaver.SaveFile(filepath, s.FreeSpace, needSpace)
}

type IFileSaver interface {
	SaveFile(filepath string, freeSpace, needSpace int) error
}

type HugeMemoryFileSaver struct {
	IFileSaver
	Next IFileSaver
}

func (hmfs *HugeMemoryFileSaver) SaveFile(filepath string, freeSpace, needSpace int) error {
	if float32(needSpace)/float32(freeSpace) < 0.5 {
		fmt.Println("Выполняю сохраниение файла в память устройства: ", filepath)
		return nil
	} else if hmfs.Next != nil {
		return (hmfs.Next).SaveFile(filepath, freeSpace, needSpace)
	}
	return fmt.Errorf("код 1: файл не был сохранен")
}

type FewMemoryFileSaver struct {
	IFileSaver
	Next IFileSaver
}

func (fmfs *FewMemoryFileSaver) SaveFile(filepath string, freeSpace, needSpace int) error {
	if float32(needSpace)/float32(freeSpace) < 1 {
		fmt.Println("Выполняю сохраниение файла в условиях ограниченной памяти устройства: ", filepath)
		fmt.Println("Предупреждаю о малом количестве оставшейся памяти")
		return nil
	} else if fmfs.Next != nil {
		return (fmfs.Next).SaveFile(filepath, freeSpace, needSpace)
	}
	return fmt.Errorf("код 2: файл не был сохранен")
}

type NoMemoryFileSaver struct {
	IFileSaver
	Next IFileSaver
}

func (nmfs *NoMemoryFileSaver) SaveFile(filepath string, freeSpace, needSpace int) error {
	if float32(needSpace)/float32(freeSpace) >= 1 {
		fmt.Println("Недостаточно памяти для сохранения файла")
		return nil
	} else if nmfs.Next != nil {
		return (nmfs.Next).SaveFile(filepath, freeSpace, needSpace)
	}
	return fmt.Errorf("код 3: файл не был сохранен")
}

func main() {
	s := Storage{
		FreeSpace: 100,
	}

	hfms := HugeMemoryFileSaver{}
	fmfs := FewMemoryFileSaver{}
	nmfs := NoMemoryFileSaver{}

	fmfs.Next = &nmfs
	hfms.Next = &fmfs
	s.FileSaver = &hfms

	s.SaveFile("filepath", 100)
}
