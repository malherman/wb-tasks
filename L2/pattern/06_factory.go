package main

import (
	"fmt"
	"math/rand"
	"time"
)

type IWarrior interface {
	Speak()
	Die()
	Attack(IWarrior)
	GetDamage(int)
	GetWarriorType() string
	SetHitPoints(int)
}

type IWarriorFactory interface {
	CreateRandomEnemy() IWarrior
	CreateOgre() IWarrior
	CreatePaladin() IWarrior
}

type WarriorFactory struct {
	IWarriorFactory
}

func (wf *WarriorFactory) CreateRandomEnemy() IWarrior {
	rand.Seed(time.Now().Unix())
	choice := rand.Int() % 2

	switch choice {
	case 0:
		return NewPaladin()
	case 1:
		return NewOgre()
	}

	return nil
}

func (wf *WarriorFactory) CreateOgre() IWarrior {
	return NewOgre()
}

func (wf *WarriorFactory) CreatePaladin() IWarrior {
	return NewPaladin()
}

type Warrior struct {
	IWarrior
	AttackPower int
	HitPoints   int
	IsAlive     bool
	Type        string
}

func (w *Warrior) SetHitPoints(hitPoints int) {
	w.HitPoints = hitPoints
}

func (p *Warrior) Speak() {
	fmt.Printf("Я %s и готов служить\n", p.Type)
}

func (p *Warrior) Die() {
	fmt.Printf("Я %s и я сдох\n", p.Type)
	p.IsAlive = false
}

func (p *Warrior) Attack(enemy IWarrior) {
	fmt.Printf("Я %s, напал на %s\n", p.Type, enemy.GetWarriorType())
	enemy.GetDamage(p.AttackPower)
}

func (p *Warrior) GetDamage(damage int) {
	fmt.Printf("Я %s, получаю урон\n", p.Type)
	p.HitPoints -= damage
	if p.HitPoints <= 0 {
		p.Die()
	}
}

func (w *Warrior) GetWarriorType() string {
	return w.Type
}

type Ogre struct {
	Warrior
}

func NewOgre() *Ogre {
	return &Ogre{
		Warrior: Warrior{
			HitPoints:   100,
			Type:        "Огр",
			AttackPower: 40,
			IsAlive:     true,
		},
	}
}

type Paladin struct {
	Warrior
}

func NewPaladin() *Paladin {
	return &Paladin{
		Warrior: Warrior{
			HitPoints:   150,
			Type:        "Паладин",
			AttackPower: 50,
			IsAlive:     true,
		},
	}
}

func main() {
	wf := WarriorFactory{}

	randWarrior := wf.CreateRandomEnemy()
	fmt.Println(randWarrior.GetWarriorType())

	ogre := wf.CreateOgre()
	paladin := wf.CreatePaladin()
	fmt.Println(ogre.GetWarriorType())
	fmt.Println(paladin.GetWarriorType())

	ogre.Attack(paladin)
	paladin.Attack(ogre)
	paladin.Attack(ogre)
}
