Что выведет программа? Объяснить вывод программы. Объяснить как работают defer’ы и порядок их вызовов.
 
package main
 
import (
    "fmt"
)
 
func test() (x int) {
    defer func() {
        x++
    }()
    x = 1
    return
}
 
 
func anotherTest() int {
    var x int
    defer func() {
        x++
    }()
    x = 1
    return x
}
 
 
func main() {
    fmt.Println(test())
    fmt.Println(anotherTest())
}

Ответ:
2 - потому что в defer используется тот x, который и будет возращен
1 - потому что defer изменяет локальный x, при возврате x, будет создана копия x, которую defer не затронет

defer вызывается после конструкции return, если таковая имеется, при выходе из функции. Вызов осуществляется в порядке от послденего к первому