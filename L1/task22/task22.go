package main

import (
	"fmt"
	"math/big"
)

func main() {
	// по условию, значения чисел > 2^20,
	// поэтому решил использовать Float из пакета math/big,
	// созданный для работы с Float произовольной точностью
	var a, b big.Float
	fmt.Print("Input a: ")
	fmt.Scan(&a)
	fmt.Print("Input b: ")
	fmt.Scan(&b)

	fmt.Println("a * b = ", new(big.Float).Mul(&a, &b))
	fmt.Println("a / b = ", new(big.Float).Quo(&a, &b))
	fmt.Println("a + b = ", new(big.Float).Add(&a, &b))
	fmt.Println("a - b = ", new(big.Float).Mul(&a, &b))
}
