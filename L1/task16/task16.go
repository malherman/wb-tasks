package main

import (
	"fmt"
	"math/rand"
	"sort"
)

func main() {
	sl := []int{}

	for i := 0; i < 20; i++ {
		sl = append(sl, rand.Int() % 100)
	}
	fmt.Println("Before:\t", sl)
	// Сортирует значения типа int в порядке возрастания
	sort.Ints(sl)
	fmt.Println("After:\t", sl)

}