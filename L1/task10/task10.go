package main

import (
	"fmt"
	"sort"
)

// находим ближайший наименьший по модулю десяток
func findMinDozen(num float64) (dozen float64) {
	if num <= -10 {
		dozen = -10
		for !(dozen > num && dozen-10 < num) {
			dozen -= 10
		}
	} else if num >= 10 {
		dozen = 10
		for !(dozen < num && dozen+10 > num) {
			dozen += 10
		}
	} else {
		dozen = 0
	}

	return
}

func main() {
	temps := []float64{-25.4, -27.0, 13.0, 19.0, 0.10, 15.5, 24.5, -21.0, 32.5}
	res := map[float64]*[]float64{}
	var dozen float64

	sort.Float64s(temps)
	// цикл заполняет мапу res в нужном виде
	for _, temp := range temps {
		// для каждого числа находит ближайший минимальный по модулю десяток
		dozen = findMinDozen(temp)
		sl, isExist := res[dozen]
		// если ключа в мапе по этому десятку еще нет
		if !isExist {
			// инициализируем слайс
			res[dozen] = &[]float64{}
			sl = res[dozen]
		}
		// добавляем температуру в нужный слайс
		*sl = append(*sl, temp)
	}

	// дальше до конца функции выполняет просто
	// красивый вывод результата
	isFirstSl := true
	for dz, sl := range res {
		if !isFirstSl {
			fmt.Print(", ")
		} else {
			isFirstSl = false
		}
		fmt.Printf("%.0f:{", dz)
		for i, temp := range *sl {
			fmt.Print(temp)
			if i != len(*sl)-1 {
				fmt.Print(", ")
			}
		}
		fmt.Print("}")
	}
	fmt.Println()
}
