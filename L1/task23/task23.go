package main

import (
	"fmt"
)

// на основе слайса sl делаем новый слайс
// содержащий все элементы из sl,
// кроме элемента на i-ой позиции
func delElemFromSlice(sl *[]int, i int) {
	*sl = append((*sl)[:i], (*sl)[i+1:]...)
}

func main() {
	sl := []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}
	delElemFromSlice(&sl, 2)
	fmt.Println(sl)
	delElemFromSlice(&sl, 7)
	fmt.Println(sl)
	delElemFromSlice(&sl, 4)
	fmt.Println(sl)
}