package main

import (
	"flag"
	"fmt"
	"os"
	"os/signal"
	"sync"
	"time"
)

var workerCount int

func init() {
	// инициализация флага для реализации возможности указывать количество воркеров
	flag.IntVar(&workerCount, "worker-count", 5, "количество воркеров")
}

type worker func(int, chan interface{}, chan interface{})

var workers []worker

func main() {
	flag.Parse()

	ch := make(chan interface{})
	finCh := make(chan struct{})
	wg := sync.WaitGroup{}

	// запуск worker-count горутин
	for i := 0; i < workerCount; i++ {
		// Указываем, что ждать на одну горуттину больше
		wg.Add(1)

		go func(ID int, ch chan interface{}, finCh chan struct{}) {
			defer func() {
				// перед завершением работы горутины
				// выводим сообщение
				fmt.Printf("Worker %d is finished\n", ID)
				// и уменьшаем счетчик горутин для ожидания
				wg.Done()
			}()

			for {
				// смотрим, из какого канал пришло сообщение
				select {
				case elem := <-ch:
					// сообщение пришло из канала с данными
					// выводим на экран
					fmt.Printf("Worker %d: %v\n", ID, elem)
					// ждем секунду, чтобы дать другим горутинам время заняться делом
					time.Sleep(time.Second)
				case <-finCh:
					return
				}
			}
		}(i+1, ch, finCh)
	}

	inputCh := make(chan interface{}) // канал для передачи считанных данных в главную функцию
	// горутина, занимающаяся чтением данных с консоли от отправкой этих данных в канал inputCH
	go func(ch chan interface{}) {
		var s string
		for {
			fmt.Scan(&s)
			ch <- s
		}
	}(inputCh)

	// после нажатия клавиш crtl+c сигнал посылается в канал stopSig
	stopSig := make(chan os.Signal)
	signal.Notify(stopSig, os.Interrupt)

	defer func() {
		// перед завершением рабобы главной горутины
		// отправляем остальным горутинам сигнал с завершением работы
		for i := 0; i < workerCount; i++ {
			finCh <- struct{}{}
		}

		// ждем завершения всех вторичных горутин
		wg.Wait()
		fmt.Println("Main worker is finished")
	}()

	for {
		select {
		case <-stopSig:
			// пришел сигнал о завершении работы
			// закругляемся
			return
		case s := <-inputCh:
			// отправляем сообщение дальше воркерам из пула
			ch <- s
		}
	}
}
