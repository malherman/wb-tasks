module wbtasks.com

go 1.16

require (
	github.com/lib/pq v1.10.4
	github.com/nats-io/gnatsd v1.4.1 // indirect
	github.com/nats-io/nats-server v1.4.1 // indirect
	github.com/nats-io/nats.go v1.13.0
	github.com/stretchr/testify v1.7.0
)
