package main

import (
	"fmt"
	"os"

	"wbtasks.com/internal/cli"
	"wbtasks.com/internal/server"
	"wbtasks.com/internal/store"
)

var (
	psqlURL string = "host=localhost user=postgres password=123 dbname=t0 sslmode=disable"
	natsURL string = "0.0.0.0:4223"
)

func main() {
	storeConfig := store.NewConfig(psqlURL)
	store := store.New(storeConfig)
	serverConfig := server.NewConfig(natsURL, storeConfig)
	server := server.New(store, serverConfig)

	if err := server.Start(); err != nil {
		fmt.Fprintln(os.Stderr, err.Error())
		return
	}

	cli := cli.New(store)
	cli.Start()
}
